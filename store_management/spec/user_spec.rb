require 'rails_helper'
 
RSpec.describe User, :type => :model do
  before do
    @user = User.new(name: "Efrain Astudillo")
  end
 
  subject { @user }
 
  describe "when name is not present" do
    before { @user.name = " " }
    it { should_not be_valid }
  end
end