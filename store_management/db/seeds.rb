# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(
[
  {
    name: "Efrain Jose",
    last: "Astudillo Vargas",
    username: "efrainastudillo",
    email: "efrain.astudillo58@gmail.com",
    password: "efrainastudillo"
  },
  {
    name: "Pedro Pablo",
    last: "Lucas Bravo",
    username: "pepaluca",
    email: "pittersdb@gmail.com",
    password: "pittersdb"
  }
])

Product.create( 
[
  { 
    code: "EX23A56", 
    description: "CERRAD. DORMIT.LAT.PULID BALLGEO",
    quantity: 23,
    cost: 23.00,
    pvp: 25.50
  },
  {
    code: "EX23A56", 
    description: "CERRAD.   DORMITORIO .A. INOX BALL GEO",
    quantity: 23,
    cost: 23.00,
    pvp: 25.50
  },
  {
    code: "EX23A56", 
    description: "CERRAD. DORMIT BALL GEO DORADA",
    quantity: 23,
    cost: 23.00,
    pvp: 25.50
  },
  {
    code: "EX23A56", 
    description: "MANIJA DORMITORIO LATA PULID DELTA GEO (CHAPA DORADA)",
    quantity: 23,
    cost: 8.53,
    pvp: 10.50
  },
  { 
    code: "EX23A56", 
    description: "CERRAD. DORMIT.LAT.PULID BALLGEO",
    quantity: 23,
    cost: 23.00,
    pvp: 25.50
  },
  {
    code: "EX23A56", 
    description: "CERRAD.   DORMITORIO .A. INOX BALL GEO",
    quantity: 23,
    cost: 23.00,
    pvp: 25.50
  },
  {
    code: "EX23A56", 
    description: "CERRAD. DORMIT BALL GEO DORADA",
    quantity: 23,
    cost: 23.00,
    pvp: 25.50
  },
  {
    code: "EX23A56", 
    description: "MANIJA DORMITORIO LATA PULID DELTA GEO (CHAPA DORADA)",
    quantity: 23,
    cost: 8.53,
    pvp: 10.50
  }
])