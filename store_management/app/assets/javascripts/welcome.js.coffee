# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

###
multiline documentation
###

ready = ->
  $("#smLoginButton").on( 'click', login_callback)
  $('li.sm-login').on('click', open_popup)
  
open_popup = (event, detail, sender) ->
  $('#myModal').modal({
    keyboard: false
  })

login_callback = (event, detail, sender) ->
  _json = {
    username: "username" , 
    password: "password"
  }
  $("#smFormLogin").submit();

  #console.log($("#smFormLogin"))
  
###
  $.ajax({
    type: "POST",
    url: '/welcome/validate_login',
    data: _json,
    success: success,
    dataType: 'json'
  });
###
success = (data) ->
  console.log(data)
  if (data.success)
    window.location.href = "store/index" 
  else
    $.growl(
      '<strong>LOGIN:</strong> User or password invalid!'
      { type: 'danger'}
    )
    
$(document).ready(ready);
$(document).on('page:load', ready);