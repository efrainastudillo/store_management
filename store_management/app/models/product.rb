class Product
  
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :code, type: String
  field :description, type: String
  
  field :brand, type: String
  field :quantity, type: Float
  
  field :cost, type: Float  # precio del producto del proveedor
  field :pvp, type: Float   # precio del producto que vendo a los clientes
                            # (tengo que ponerlo porque a veces se redondea los valores)
                            
  field :utility, type: Float # el porcentaje de utilidad
  
  has_and_belongs_to_many :option_types
  has_many :variants
  belongs_to :store
end