class Store
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, type: String
  has_many :products
  has_many :users
end