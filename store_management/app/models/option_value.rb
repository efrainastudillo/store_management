class OptionValue
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, type: String
  belongs_to :option_type
  belongs_to :variant
end