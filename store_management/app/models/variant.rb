class Variant
  include Mongoid::Document
  include Mongoid::Timestamps
  
  
  belongs_to :product
  has_many :option_values
end