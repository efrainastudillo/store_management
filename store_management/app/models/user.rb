class User
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, type: String
  field :last, type: String
  field :username, type: String
  field :email, type: String
  field :password, type: String
  
  belongs_to :store
  #def deserialize(object)
    #super(object).try(:to_datetime) # Call deserialize in Timekeeping
    #end
end
