class OptionType
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, type: String
  
  has_and_belongs_to_many :products
  has_many :option_values
  
end