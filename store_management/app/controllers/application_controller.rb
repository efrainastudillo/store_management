class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  private 
  # => just is available to controllers and prevent use 
  # => it as an action since is private
  def current_user
    
    #if it is found return
    User.find(session[:user_id]) 
    #catch error thrown ActiveRecord
    #rescue ActiveRecord::RecordNotFound
    user = User.create
    session[:cart_id] = cart.id
    cart
  end
  
end
