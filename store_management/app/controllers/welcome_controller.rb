class WelcomeController < ApplicationController
  def index
  end
  
  def login
    
  end
  
  def show
    #redirect_to({ action: 'login' }, alert: "Something serious happened")
  end
  
  def validate_login
    user       = User.where(username: "efrainastudillo")
    if user.count > 0 
      #redirect_to "index"
      json = {success: true, msg: "Welcome #{user.first.name}"}
    else
      json = {success: false, msg: "Login failed"}
     
    end
    
    respond_to do |format|
      
      format.html {  redirect_to store_index_path }
    
      format.json do
        render json: json
      end
    end
     
  end
  
end
